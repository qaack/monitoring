import requests
from influxdb import InfluxDBClient
from datetime import datetime
import yaml

# Configuration
instance_url = None
token = None
influxdb_host = None
influxdb_port = None
influxdb_database = None

# get config from a yaml config file
with open('config.yaml') as f:
    config = yaml.safe_load(f)

    instance_url = config['instance_url']
    token = config['token']
    influxdb_host = config['influxdb']['host']
    influxdb_port = config['influxdb']['port']
    influxdb_database = config['influxdb']['database']

client = InfluxDBClient(host=influxdb_host, port=influxdb_port)
endpoints = {
    'users': '/api/metrics/users',
    'repos': '/api/metrics/repos',
    'envs': '/api/metrics/envs',
    'services': '/api/metrics/services',
}

for name,path in endpoints.items():
    # Fetch JSON data from the endpoint
    headers = {'X-QAACK-TOKEN': token}
    response = requests.get(instance_url+path, headers=headers)
    data = response.json()

    # check response was ok
    if response.status_code != 200:
        print(f'Error fetching data: {data["error"]}')
        continue


    # Prepare data for InfluxDB with current timestamp
    current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    influxdb_data = [
        {
            'measurement': name,
            'tags': {
                'instance': instance_url,  # Replace with your instance URL
            },
            'time': current_time,  # Set the time to the current timestamp
            'fields': data
        }
    ]

    # Connect to InfluxDB and write the data
    client.switch_database(influxdb_database)
    client.write_points(influxdb_data)

# Close the InfluxDB connection
client.close()
